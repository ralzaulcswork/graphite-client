package com.ralzaul.graphiteclient;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GraphiteClientTest {

	String graphiteHost = "";
	Integer graphitePort = 2003;

	@Mock
	private Socket socket;

	private ByteArrayOutputStream out;
	private GraphiteClient graphiteClient;
	private long currentTimestamp;

	@Before
	public void setUp() throws IOException {
		out = new ByteArrayOutputStream();
		currentTimestamp = System.currentTimeMillis() / 1000;

		graphiteClient = new GraphiteClient(graphiteHost, graphitePort) {
			@Override
			protected Socket createSocket() {
				return socket;
			}
		};

		when(socket.getOutputStream()).thenReturn(out);
	}

	@Test
	public void testSendSingleIntegerMetric() throws IOException {
		graphiteClient.sendMetric("junit.test.metric", 4711, 1l);
		assertEquals(String.format("junit.test.metric 4711 1%n"), out.toString());
	}

	@Test
	public void testSendSingleDoubleMetric() throws IOException {
		graphiteClient.sendMetric("junit.test.metric", 47.11, 1l);
		assertEquals(String.format("junit.test.metric 47.11 1%n"), out.toString());
	}

	@Test
	public void testSendSingleFloatMetric() throws IOException {
		graphiteClient.sendMetric("junit.test.metric", 47.11f, 1l);
		assertEquals(String.format("junit.test.metric 47.11 1%n"), out.toString());
	}

	@Test
	public void testSendMultipleDoubleMetrics() {
		Map<String, java.lang.Double> data = new HashMap<String, Double>();
		data.put("junit.test.metric1", 47.11);
		data.put("junit.test.metric2", 47.12);
		graphiteClient.sendMetrics(data);
		assertTrue(out.toString().contains("junit.test.metric1 47.11 " + currentTimestamp));
		assertTrue(out.toString().contains("junit.test.metric2 47.12 " + currentTimestamp));
	}

	@Test
	public void testSendMultipleIntegerMetrics() {
		Map<String, Integer> data = new HashMap<String, Integer>();
		data.put("junit.test.metric1", 4711);
		data.put("junit.test.metric2", 4712);
		graphiteClient.sendMetrics(data);
		assertTrue(out.toString().contains("junit.test.metric1 4711 " + currentTimestamp));
		assertTrue(out.toString().contains("junit.test.metric2 4712 " + currentTimestamp));
	}

	@Test
	public void testSendMultipleFloatMetrics() {
		Map<String, Float> data = new HashMap<String, Float>();
		data.put("junit.test.metric1", 47.11f);
		data.put("junit.test.metric2", 47.12f);
		graphiteClient.sendMetrics(data);
		assertTrue(out.toString().contains("junit.test.metric1 47.11 " + currentTimestamp));
		assertTrue(out.toString().contains("junit.test.metric2 47.12 " + currentTimestamp));
	}

	@Test
	public void testSendSingleMetricCurrentTime() throws IOException, InterruptedException {
		graphiteClient.sendMetric("junit.test.metric", 4711);
		assertEquals(String.format("junit.test.metric 4711 %d%n", currentTimestamp), out.toString());
	}

}