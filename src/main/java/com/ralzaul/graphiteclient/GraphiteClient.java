package com.ralzaul.graphiteclient;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.*;import java.lang.Number;import java.lang.RuntimeException;import java.lang.String;import java.lang.SuppressWarnings;import java.lang.System;import java.lang.Thread;import java.lang.Throwable;import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * Thin java client for writing data to a Graphite Server.
 * Some of the features include:
 * <p/>
 * - sending a single metric
 * - sending a set of metrics
 * - sending metrics with a delay (for slow servers)
 * - sending metrics using any type that inherits from Number interface
 *
 * @author ralzaul
 */

public class GraphiteClient {

	private static final int DEFAULT_DELAY_IN_MS = 50;
	private final String graphiteHost;
	private final int graphitePort;
	private final int delayInMsc;

	/**
	 * Create a new GraphiteClient.
	 *
	 * @param graphiteHost host where the graphite server is running.
	 * @param graphitePort port that the graphite server is listening to
	 */
	public GraphiteClient(String graphiteHost, int graphitePort, int delayInMsc) {
		this.graphiteHost = graphiteHost;
		this.graphitePort = graphitePort;
		this.delayInMsc = delayInMsc;
	}

	/**
	 * Create a new GraphiteClient.
	 *
	 * @param graphiteHost host where the graphite server is running.
	 * @param graphitePort port that the graphite server is listening to
	 */
	public GraphiteClient(String graphiteHost, int graphitePort) {
		this.graphiteHost = graphiteHost;
		this.graphitePort = graphitePort;
		this.delayInMsc = DEFAULT_DELAY_IN_MS;
	}

	/**
	 * Send a set of metrics with the current time as timestamp to graphite.
	 *
	 * @param metrics the metrics inside an Object that implements the Map interface
	 */
	public <T extends Number> void sendMetrics(Map<String, T> metrics) {
		sendMetrics(metrics, System.currentTimeMillis() / 1000);
	}

	/**
	 * Send a set of metrics with a given timestamp to graphite.
	 *
	 * @param metrics   the metrics as key-value-pairs
	 * @param timeStamp the timestamp
	 */
	public <T extends Number> void sendMetrics(Map<String, T> metrics, long timeStamp) {
		try {

			Socket socket = createSocket();
			OutputStream s = socket.getOutputStream();
			PrintWriter out = new PrintWriter(s, true);

			for (Map.Entry<String, T> metric : metrics.entrySet()) {
				String outString = metric.getKey() + " " + String.valueOf(metric.getValue());
				out.printf(outString + " %d%n", timeStamp);
				Thread.sleep(delayInMsc);
			}

			out.close();
			socket.close();

		} catch (UnknownHostException e) {
			throw new GraphiteException("Unknown host: " + graphiteHost);
		} catch (java.lang.InterruptedException e) {
			throw new GraphiteException("process interrupted for delay : " + delayInMsc + "ms");
		} catch (IOException e) {
			throw new GraphiteException("Error while writing data to graphite: " + e.getMessage(), e);
		}
	}

	/**
	 * Send a single metric with the current time as timestamp to graphite.
	 *
	 * @param key   The metric key
	 * @param value the metric value
	 * @throws GraphiteException if writing to graphite fails
	 */
	public <T extends Number> void sendMetric(String key, T value) {
		sendMetric(key, value, System.currentTimeMillis() / 1000);
	}

	/**
	 * Send a single metric with a given timestamp to graphite.
	 *
	 * @param key       The metric key
	 * @param value     The metric value
	 * @param timeStamp the timestamp to use
	 * @throws GraphiteException if writing to graphite fails
	 */
	@SuppressWarnings("serial")
	public <T extends Number> void sendMetric(final String key, final T value, long timeStamp) {
		sendMetrics(new HashMap<String, T>() {{
			put(key, value);
		}}, timeStamp);
	}

	protected Socket createSocket() throws UnknownHostException, IOException {
		return new Socket(graphiteHost, graphitePort);
	}

	class GraphiteException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public GraphiteException(String message) {
			super(message);
		}

		public GraphiteException(String message, Throwable cause) {
			super(message, cause);
		}
	}
}