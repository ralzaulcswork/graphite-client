# graphiteclient

A simple java client for sending metrics to graphite. 

## Usage: 

// For simple graphite client object 
SimpleGraphiteClient graphiteClient = new SimpleGraphiteClient("my_graphite_host", 2003);

// For simple graphite client object with 50ms delay in between requests
SimpleGraphiteClient graphiteClient = new SimpleGraphiteClient("my_graphite_host", 2003, 50);

// send single value with current timestamp
graphiteClient.sendMetric("universal.answer", 42);

// send single value with custom timestamp
graphiteClient.sendMetric("universal.answer", 42, 1360848777l);

// send multiple values
Map<String, Integer> allAnswers = new HashMap<String, Integer>() {{
	put("where.is.my.towel", 42);
	put("where.are.the.dolphins", 42);
}};
graphiteClient.sendMetrics(allAnswers);

